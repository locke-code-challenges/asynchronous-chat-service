package de.lockenvitz.asynchronousChatService.service;

import de.lockenvitz.asynchronousChatService.model.Post;
import de.lockenvitz.asynchronousChatService.model.User;
import de.lockenvitz.asynchronousChatService.model.UserPost;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserDetailServiceTest extends BaseTest {

    @InjectMocks
    private UserDetailService serviceUnderTest;

    private UserService userService;
    private PostService postService;

    @BeforeEach
    public void setUp() {
        userService = mock(UserService.class);
        postService = mock(PostService.class);
        serviceUnderTest = new UserDetailService(userService, postService);
    }

    @Test
    void collectUserPost_should_return_correct_list_of_UserPost() {
        final List<UserPost> expectedUserPostList = List.of(new UserPost(USER, POST_LIST));
        final CompletableFuture<User> userCompletableFuture = CompletableFuture.completedFuture(USER);
        final CompletableFuture<List<Post>> listCompletableFuture = CompletableFuture.completedFuture(POST_LIST);

        when(userService.collectUserData(anyInt())).thenReturn(userCompletableFuture);
        when(postService.collectPostData(anyInt())).thenReturn(listCompletableFuture);

        List<UserPost> result = serviceUnderTest.collectUserPost(1);

        assertThat(result.size(), is(1));
        assertThat(result, is(expectedUserPostList));
    }
}