package de.lockenvitz.asynchronousChatService.service;

import de.lockenvitz.asynchronousChatService.model.*;

import java.util.List;

public class BaseTest {

    protected static final String USER_API_URL = "http://jsonplaceholder.typicode.com/users/1";
    protected static final String POST_API_URL = "http://jsonplaceholder.typicode.com/posts?userId=2";

    protected static final Geo GEO = new Geo("123", "321");
    protected static final Address ADDRESS = new Address("street", "suite", "city", "zip", GEO);
    protected static final Company COMPANY = new Company("name", "phrase", "bs");
    protected static final User USER = new User(1, "name", "user", "email", ADDRESS, "phone", "website", COMPANY);
    protected static final List<Post> POST_LIST = List.of(
            new Post(1, 1, "titel 1", "body 1"),
            new Post(2, 1, "titel 2", "body 2")
    );
    protected static final String USER_API_JSON_RESPONSE = "{\n" +
            "  \"id\": 1,\n" +
            "  \"name\": \"name\",\n" +
            "  \"username\": \"user\",\n" +
            "  \"email\": \"email\",\n" +
            "  \"address\": {\n" +
            "    \"street\": \"street\",\n" +
            "    \"suite\": \"suite\",\n" +
            "    \"city\": \"city\",\n" +
            "    \"zipcode\": \"zip\",\n" +
            "    \"geo\": {\n" +
            "      \"lat\": \"123\",\n" +
            "      \"lng\": \"321\"\n" +
            "    }\n" +
            "  },\n" +
            "  \"phone\": \"phone\",\n" +
            "  \"website\": \"website\",\n" +
            "  \"company\": {\n" +
            "    \"name\": \"name\",\n" +
            "    \"catchPhrase\": \"phrase\",\n" +
            "    \"bs\": \"bs\"" +
            "   }\n" +
            "}";
    protected static final String POST_API_JSON_RESPONSE = "[\n" +
            "  {\n" +
            "    \"userId\": 1,\n" +
            "    \"id\": 1,\n" +
            "    \"title\": \"titel 1\",\n" +
            "    \"body\": \"body 1\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"userId\": 1,\n" +
            "    \"id\": 2,\n" +
            "    \"title\": \"titel 2\",\n" +
            "    \"body\": \"body 2\"\n" +
            "  }]";
}
