package de.lockenvitz.asynchronousChatService.service;

import de.lockenvitz.asynchronousChatService.model.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class PostServiceTest extends BaseTest {

    private final RestTemplate restTemplate = new RestTemplate();
    private PostService serviceUnderTest;
    private MockRestServiceServer mockServer;

    @BeforeEach
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        serviceUnderTest = new PostService(restTemplate);
    }

    @Test
    void collectPostData_should_return_correct_post_object() throws ExecutionException, InterruptedException {

        final CompletableFuture<List<Post>> expectedPostList = CompletableFuture.completedFuture(POST_LIST);

        mockServer.expect(once(), requestTo(POST_API_URL)).andRespond(
                withSuccess(POST_API_JSON_RESPONSE, MediaType.APPLICATION_JSON)
        );

        final CompletableFuture<List<Post>> result = serviceUnderTest.collectPostData(2);

        assertThat(result.get(), is(expectedPostList.get()));
        assertThat(result, instanceOf(CompletableFuture.class));
    }
}