package de.lockenvitz.asynchronousChatService.service;

import de.lockenvitz.asynchronousChatService.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class UserServiceTest extends BaseTest {

    private final RestTemplate restTemplate = new RestTemplate();
    private UserService serviceUnderTest;
    private MockRestServiceServer mockServer;

    @BeforeEach
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        serviceUnderTest = new UserService(restTemplate);
    }

    @Test
    void collectUserData_should_return_correct_user_object() throws ExecutionException, InterruptedException {
        final CompletableFuture<User> expectedUser = CompletableFuture.completedFuture(USER);

        mockServer.expect(once(), requestTo(USER_API_URL)).andRespond(
                withSuccess(USER_API_JSON_RESPONSE, MediaType.APPLICATION_JSON)
        );

        final CompletableFuture<User> result = serviceUnderTest.collectUserData(1);

        assertThat(result.get(), is(expectedUser.get()));
        assertThat(result, instanceOf(CompletableFuture.class));
    }
}