package de.lockenvitz.asynchronousChatService.model;

import java.util.List;
import java.util.Objects;

public class UserPost {

    private final User userDetails;
    private final List<Post> postList;

    public UserPost(
            final User userDetails,
            final List<Post> postList) {
        this.userDetails = userDetails;
        this.postList = postList;
    }

    public User getUserDetails() {
        return userDetails;
    }

    public List<Post> getPostList() {
        return postList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPost userPost = (UserPost) o;
        return Objects.equals(userDetails, userPost.userDetails) &&
                Objects.equals(postList, userPost.postList);
    }
}
