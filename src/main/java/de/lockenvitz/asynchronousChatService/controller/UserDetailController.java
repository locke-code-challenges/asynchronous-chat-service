package de.lockenvitz.asynchronousChatService.controller;

import de.lockenvitz.asynchronousChatService.model.UserPost;
import de.lockenvitz.asynchronousChatService.service.UserPostCollectorInterface;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserDetailController {

    private static final int PASSING_MAX_COUNT = 10;

    private final UserPostCollectorInterface userDetailService;

    public UserDetailController(UserPostCollectorInterface userDetailService) {
        this.userDetailService = userDetailService;
    }

    //todo: Improvement: Add Integration or/and unit test for RestController

    @RequestMapping(value = "/userdetails", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<UserPost> getUserDetails() {
        return userDetailService.collectUserPost(PASSING_MAX_COUNT);
    }
}