# Employee Management Service

Service which consumes asynchronously JSON response of two REST endpoints, merge them and offer it via a own REST API 
as response.

## Features
* offer an own REST interface 
* grab JSON data from two different REST API
* grab this information asynchronously
* merge the JSON data to one response

## Challenge

**Fetch asynchronous data from an API**

Your task is to write an app, which gathers data from two endpoints asynchronously, merges the responses and displays 
them in any way, for example as JSON response from an REST API. Please try to not use blocking calls throughout your 
solution.

For example you could use these two endpoints:

http://jsonplaceholder.typicode.com/users/1 to obtain a user’s data

http://jsonplaceholder.typicode.com/posts?userId=1 to obtain all comments written by that user

**Testing**

Please write (at least one) test that shows that your implementation works. Please work on as much as you personally 
like in affordable time. If you have time left over, we are happy if you improve your solution in a direction that you 
think fits the challenge. Please rather deliver less in high quality than more in lesser quality. Your code should be 
stored in an public git repository with a generic name. Please commit frequently. Please do not include the descriptions 
of the tasks in your repository.

## Requirements for Linux user 

### Setup

* Build and run the app using maven

$ `mvn clean install spring-boot:run`

## Explore the REST API

The service is reachable at `http://localhost:9000` and offers the following endpoints.

### Endpoints

* Get information of a user and associated messages

`GET /userdetails`